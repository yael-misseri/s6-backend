let endpoints_import = require('./endpoints');

class standarduserEndpoints extends endpoints_import.endpoints{

  constructor(standarduserRoutes,standarduserModel){
    super(standarduserRoutes,standarduserModel);
  }

  init(){

    super.init();

  }

}

module.exports.entityEndpoints = standarduserEndpoints;
