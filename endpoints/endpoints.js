const entityModel = require('../models/entity.model');
const Security = require ('../security/security');

/**
enpoints provides generic HTTP endpoints and functions to perform generic operations on each model.
This class is directly in relation with MongoDB via Mongoose, using a generic CRUD
@version 1.0.0
*/
class endpoints{

  /**
  Only called by the child class
  @param {Express Router} routes - Specific router for the calling models
  @param {Mongoose Model} model - Specific model  of the calling child class
  */
  constructor(routes,model){

    this.routes = routes;
    this.model = model;
    this.entityType = model.collection.collectionName.substring(0, model.collection.collectionName.length - 1); //Name of the entity model (eg. "user")

  }


  //Genreic CRUD :

  /**
  Save a new object in DB. Generic 'C' of CRUD. This function will create a new object of the type of the current model and a new "entity" object related to the new object.
  @param data {Array} - Attributes of the object (please refer to the current model in 'models' directory)
  @param creatorid {Integer} - The userid of the creator
  @param ownerid {Integer} - The userid of the owner (=creatorid in a lot of cases)
  @param res {Express Request Result} - [OPTIONNAL] if specified, the result of this function will be send through the request and sent to client. Only one res object can be fulfill for one request, so use it carefully and only when the response must directly be sent to the client.
  @return {Object} - The saved object in DB, or null if the operation fails
  */
  async addRecord(data, creatorid, ownerid, res=null){

    var current = this;
    var model = this.model;
    var entityType = this.entityType;

    let randomEntityId = await Security.generateRandomId(); //Generates a random id for the entity

    if(entityType==="user"){ //If a new user is creating, the  current user id is the new created user id (the randomEntityId generated before)
      console.log("New user creation")
      creatorid = randomEntityId;
      ownerid = randomEntityId;
      data.password = Security.hashPassword(data.password);

      if(await Security.userEmailAlreadyExists(data.email)){
        console.log("New user creation : Error email already used !")
        if(res){res.status(400).json('email already used !');}
        return null;
      }

    }

    await current.createEntity(res,entityType,creatorid,ownerid, randomEntityId);

    let object = new model(data);
    object[entityType+"id"] = randomEntityId; //Give the object the same id as the entity object created just before
    await object.save().
      then(object => {
        console.log(entityType+' added avec succes');
        if(res){res.status(200).json({'object': entityType+' added avec succes'} ); }
        return true;

      })
      .catch(err => {
        console.log('add ' + entityType + ' failed');
        if(res){res.status(400).json('add ' + entityType + ' failed');}
        return false;
      });

     return object;

  }

  /**
  Get an existing entity in DB. The 'R' of CRUD. Checks if the object is deleted or not. If deleted, it fails.
  @param entityid {Integer} - The entityid of the wanted object. The entityid of an object corresponds to the model key (eg. for an user it is 'userid'), and is the same as the 'entityid' attribute of the related "entity" object in "entities" collection.
  @param res {Express Request Result} - [OPTIONNAL] if specified, the result of this function will be send through the request and sent to client. Only one res object can be fulfill for one request, so use it carefully and only when the response must directly be sent to the client.
  @return {Object} - The retrieved entity in DB, or null if the operation fails
  */
  static async getEntity(entityid, res=null){

    var current = this;
    var model = this.model;

    var foundEntity = null;

    if(typeof entityid!="number" && typeof entityid!="string"){
      if(res){res.status(404).send("main entity not found");}
      return null;
    }

    await entityModel.find({ entityid: entityid },

      function(err,entity){

        if(entity.length==0 || !entity){
          console.log('entity not found (getEntity function)');
          if(res){res.status(404).send("main entity not found");}
        } else {
          if(entity[0].deleted){
            console.log('entity not found deleted (getEntity function)');
            if(res){res.status(404).send('main entity deleted');}
          }else{
            console.log('entity fpund success (getEntity function)');
                foundEntity = entity[0];
                if(res){res.status(200).json(entity[0])};
            };
          }

        }

    )
    return foundEntity;

  }

  async getRecord(entityid, res=null){

     var current = this;
     var model = this.model;
     var entityType = this.entityType;

     var foundEntity = null
     var foundObject = null;

    if(typeof entityid!="number" && typeof entityid!="string"){
       if(res){res.status(404).send("main entity not found");}
       return null;
     }

     //Looking for the main entity ---------------------------------
     await entityModel.find({ entityid: entityid },

       function(err,entity){

         if(typeof entity=="undefined" || entity.length==0){
           console.log('entity not found (getRecord function)');
           if(res){res.status(404).send("main entity not found");}
         } else {
           if(entity[0].deleted){
             console.log('entity not found deleted (getRecord function)');
             if(res){res.status(404).send('main entity deleted');}
           }else{
             console.log('entity found success (getRecord function)');
                 foundEntity = entity[0];
             };
           }

         }

     ); //-----------------------------------------------------

     if(foundEntity==null){return null;}

     //Then, the object ---------------------------------------
     var model_id_name = entityType + "id"; //The primary key of the object

     await model.find({[model_id_name]:foundEntity.entityid},

     (err, object) => {

      if(object.length==0 || !object){
        console.log('object not found (getRecord function)');
        if(res){res.status(404).send("object not found");}
      } else{
        console.log("object found !" )

        foundObject = object[0];
        if(res){res.status(200).send(object[0])};
      }

    }); //-----------------------------------------------------

     return foundObject;

   }


  /**
  Update an existing object in DB. The 'U' of CRUD. Checks if the object is deleted or not. If deleted, it fails. Updates the related "entity" object in order to change the updated date.
  @param entityid {Integer} - The entityid of the wanted object. The entityid of an object corresponds to the model key (eg. for an user it is 'userid'), and is the same as the 'entityid' attribute of the related "entity" object in "entities" collection.
  @data {Array} - Attributes of the object (please refer to the current model in 'models' directory)
  @param res {Express Request Result} - [OPTIONNAL] if specified, the result of this function will be send through the request and sent to client. Only one res object can be fulfill for one request, so use it carefully and only when the response must directly be sent to the client.
  @return {Object} - The updated object in DB, with new values, or null if the operation fails
  */
  async updateRecord(entityid, data, res=null){

    var current = this;
    var model = this.model;
    var entityType = this.entityType;

    var foundEntity = null;
    var foundObject = null;
    var updatedObject = null;

    if(typeof entityid!="number" && typeof entityid!="string"){
      if(res){res.status(404).send("main entity not found");}
      return null;
    }

    //Looking for the entity ----------------------------------
    await entityModel.find({ entityid: entityid },

      function(err,entity){

        if(entity.length==0 || !entity){
          console.log('main entity not found (updateRecord function)');
          if(res){res.status(404).json("entity not found");}
        } else {
          if(entity[0].deleted){
            console.log('main entity not found deleted  (updateRecord function)');
            if(res){res.status(404).json('entity not found deleted');}
          }else{
            foundEntity = entity[0];
          }
        }
      });
      //-------------------------------------------------------

      if(foundEntity==null){
        return null;
      }

      //use a dynamic array key
      var model_id_name = entityType + "id";

      //Then the object ---------------------------------------
      await model.find({[model_id_name]: foundEntity.entityid}, function(err, object) {

        if(object.length==0 || !object){
          console.log('object not found (updateRecord function)');
          if(res){res.status(404).json("not found");}

        }else{

          foundObject = object[0];
          object[0].schema.eachPath(function(attribute){

            if(data[attribute]){
              object[0][attribute] = data[attribute];
            }
          });

        }

      }); //---------------------------------------------------

      if(foundObject==null){
        return null;
      }

      //Finnaly, update the object ----------------------------
      await foundObject.save().then(object => {
        console.log('model updated (updateRecord function)');
        if(res){res.status(200).json('model updated');}
        updatedObject = object;
      })
      .catch(err => {
        console.log('update impossible (updateRecord function)');
        if(res){res.status(400).json("Update impossible");}
      }); //---------------------------------------------------

      if(updatedObject!=null){
        foundEntity.updatedtime = new Date();
        await foundEntity.save();
      }

      return updatedObject;

  }

  /**
   Deletes an existing object in DB. The 'D' of CRUD. Directly call the "updateRecord" function to set the "deleted" attribute of the related "entity" object to "true". The object is still in base after deleting.
  @param entityid {Integer} - The entityid of the wanted object. The entityid of an object corresponds to the model key (eg. for an user it is 'userid'), and is the same as the 'entityid' attribute of the related "entity" object in "entities" collection.
  @param res {Express Request Result} - [OPTIONNAL] if specified, the result of this function will be send through the request and sent to client. Only one res object can be fulfill for one request, so use it carefully and only when the response must directly be sent to the client.
  @return {Object} - The deleted object in DB, or null if the operation fails
  */
  async deleteRecord(entityId, res=null){

    var data = {deleted:true};
    var deletedObject = await this.updateRecord(entityId,data);

    if(res && deletedObject==null){
      console.log('delete entty impossible (deleteRecord function)');
      res.status(400).json('delete impossible');
    }else if(res){
      console.log('entity deleted (deleteRecord function)');
      res.status(200).json('object deleted');
    }

    return deletedObject;

  }


  /**
  Separated function of "addRecord" function, useful to create the related "entity" object of a new object.
  @param res {Express Request Result} - [OPTIONNAL] if specified, the result of this function will be send through the request and sent to client. Only one res object can be fulfill for one request, so use it carefully and only when the response must directly be sent to the client.
  @param entityType {String} - The name of the model of the entity
  @param creatorid {Integer} - The userid of the creator
  @param ownerid {Integer} - The userid of the owner (=creatorid in a lot of cases)
  @param entityid {Integer} - The id of the new created object (= eg. userid).
  @param description {String} - [OPTIONNAL] A description for the new object
  @param visibility {String} - [OPTIONNAL] Set the visibility attribute
  */
  createEntity(res=null,entityType,creatorid,ownerid,entityid,description=null,visibility="default"){

    let entity = {

      "entityid":entityid,
      "type": entityType,
      "description": description,
      "ownerid": ownerid,
      "creatorid": creatorid,
      "deleted":false,
      "visibility": "default"

    }

    let object = new entityModel(entity);
    object.save()
      .then(object => {
        console.log("Entity created");
      })
      .catch(err => {
        console.log("Entity creation failed");
        if(res){
          res.status(400).send('add entity failed');
          res.end();
        }
      });

  }


  /**
  Main function to init generic routes
  */
 init(){

   //If no routes, no endpoints possible, only CRUD DB operations
   if(this.routes==null){ return;}

    var current = this;
    var model = this.model;



    //Read a specific record of type "model"
    this.routes.route('/read/:token/:userId/:id').get(async function(req, res) {

    	let entityid = req.params.id;
      let token = req.params.token;
      let userid = req.params.userId;

      let foundObject = await current.getRecord(entityid);

      if(foundObject==null){
        res.status(404).json('not found');

      }else if(await Security.isOwner(entityid,userid) && await Security.checkUserToken(token,userid)){
        res.status(200).json(foundObject);

      }else{
        console.log('an access has been denied (route /read)');
        res.status(403).json('access denied');
      }

    });


    //Create a new record of type "model"
    this.routes.route('/add/:token/:userid').post(async function(req, res) {

      let token = req.params.token;
      let userid = req.params.userid; //If the user is not connected he has id 0

      if(current.entityType=="user"){
        await current.addRecord(req.body, userid, userid, res);
      }else if(await Security.checkUserToken(token,userid)){
        await current.addRecord(req.body, userid, userid, res);
      }else{
        res.status(403).json('forbiden')
      }

    });

    //Update a specific record of type "model"
    this.routes.route('/update/:token/:userId/:id').post(async function(req, res) {

      let entityid = req.params.id;
      let token = req.params.token;
      let userid = req.params.userId;

      let foundObject = await current.getRecord(entityid);

      if(foundObject==null){
        res.status(404).json('not found');
      }

      if(await Security.isOwner(entityid,userid) && await Security.checkUserToken(token,userid)){
        current.updateRecord(entityid, req.body, res);
      }else{
        console.log('an access has been denied (route /update)');
        res.status(403).json('access denied');
      }

    });
  }

}

module.exports.endpoints = endpoints;
