let endpoints_import = require('./endpoints');
const Security = require ('../security/security');

/**
userEnpoint provides specifics HTTP endpoints to perform specific operations on the 'users' collectionName
Extends the main endpoints class "endpoints"
@version 1.0.0
@see <a href="endpoints.js.html">endpoints class</a>
*/
class userEndpoints extends endpoints_import.endpoints{



  /**
  The constructor only calls the super-constructor and gives these two parameters to set up all generics endpoints an function for the current model.
  @param userRoutes {Express Router} - specicif router for this model
  @param userModel {Mongoose Model} - model of user with attributes
  @see <a href="endpoints.js.html">endpoints class</a>
  */
  constructor(userRoutes,userModel){
    super(userRoutes,userModel);
  }

  /**Try authentification by checking password. Use Security class.
   @param user = DB object
   @param password = password provided by client
   @return {Boolean} (true: connexion succed false: connexion failed)
   @see <a href="security.js.html">Security class</a>*/
  _tryConnection(user,raw_password){

    let hashed_password = user.password;
    return Security.compareHash(raw_password,hashed_password);

  }

  /**
  Disconnect the user. Checks if the token is good, to certify the user has the permission to disconect.
  Replace the current token in DB by a new one, to make the token unusable.
  @param {Object} userid - The user to disconnect (his id)
  @param {String} token - User's token
  @return {Boolean} disconnection agreed or forbiden
  */
  async _logOutUser(userid,token){

    if(await Security.checkUserToken(token,userid)){
      if(await this.updateRecord(userid, {token:Security.generateToken(),isconnected:"false"})){
        return true;
      }
    }

    return false;

  };

  /**After connexion, generates a token for the user session. Stores the token in DB. Use Security class.
  //@param {Object}  user DB Object of collection 'user'
  //@return {String} The generated token
  //@see <a href="security.js.html">Security class</a>*
  */
  _generateUserToken(user){

    let token = Security.generateToken();
    let entityid= user.userid;

    //Data array for update
    let data = {token:token, isconnected:true};
    this.updateRecord(entityid, data);

    return token;
  }


  /**
  Main function to init the object routes
  */
  async init(){

    super.init();
    var current = this;

    //route for user login
    this.routes.route('/login/:token/:userid').post(async function(req, res) {

      let login_email = req.body.email;
      let login_password = req.body.password;

      await current.model.find({ email: login_email },

        async function(err,user){
          if(user.length==0){
            console.log("user not found");
      			res.status(404).json("user not found");
      		} else {
            if(await !current._tryConnection(user[0],login_password)){
              console.log("bad logins for user ");
              res.status(403).json('bad login');
            }else{
              console.log("login: success");

              res.status(200).json({token:current._generateUserToken(user[0]),userid:user[0].userid});
            }

          }

        }

      )

    });

    //route for logout
    this.routes.route('/logout/:token/:userid').get(async function (req,res){

      if(await current._logOutUser(req.params.userid,req.params.token)){
        res.status(200).json('Disconnected with sucess');
      }else{
        res.status(400).json('Error - can\'t disconnected this user')
      }

    });

  }

}

module.exports.userEndpoints = userEndpoints;
