const entityModel = require('../models/entity.model');

const endpoints_import = require('../endpoints/endpoints');

let userModel = require('../models/user.model');
const user_endpoints_import = require('../endpoints/user.endpoints');


class security{

  constructor(){
    this.passwordHash = require('password-hash');
  }

  hashPassword(raw_password){
    return this.passwordHash.generate(raw_password);
  }

  compareHash(raw_password,hashed_password){
    return this.passwordHash.verify(raw_password,hashed_password);
  }

  generateToken(){

    let token= "@token=";
    for(let i=0; i<10; i++){
      token += Math.random().toString(36).substring(7);
    }
    return token;

  }

  async generateRandomId(){

    var current_time= new Date().getUTCMilliseconds();
    var random_int = Math.floor(Math.random() * Math.floor(10000));

    let randomId = current_time * random_int;

    if(await this.entityIdAlreadyExists(randomId)){
      console.log("ENTITY ID ALREADY EXISTS !!!!");
      return this.generateRandomId();
    }

    return randomId;
  }

  /**
  Checks if the current connected user's token is valid. Compares the provided token to the saved token in DB for the user current_user_id.
  @param token {String} - provided token
  @param current_user_id {Integer} - The actual connected user id
  @param timeout {Integer} - Max number of call for the recusrivity, in case of busy process
  @return {Boolean} - Is the token valid ?
  */
  async checkUserToken(token, current_user_id){

    let userClass = new user_endpoints_import.userEndpoints(null,userModel);
    let user = await userClass.getRecord(current_user_id);
    if(!user){return false;}
    if(token==user.token){return true}
    return false;
  }

  /**
  Checks if the current connected user is the owner of the object represented by "entityid"
  @param entityid {Integer} - The id of the wanted object
  @param current_user_id {Integer} - The actual connected user id
  @return {Boolean} - Is the user owner ?
  */
  async isOwner(entityid, current_user_id){

    current_user_id = parseInt(current_user_id);
    let entity = await endpoints_import.endpoints.getEntity(entityid);
    if(!entity){return false}
    let ownerid = entity.ownerid;
    return ownerid == current_user_id;

  }

  /**
  Checks if the current connected user is the creator of the object represented by "entityid"
  @param entityid {Integer} - The id of the wanted object
  @param current_user_id {Integer} - The actual connected user id
  @return {Boolean} - Is the user creator ?
  */
  async isCreator(entityid, current_user_id){

    current_user_id = parseInt(current_user_id);
    let entity = await endpoints_import.endpoints.getEntity(entityid);
    if(!entity){return false}
    let creatorid = entity.creator;
    return creatorid == current_user_id;

  }

  /**
  Checks if the provided entityid exists in DB
  @param entityid {Integer} - An id of object
  @return {Boolean} - This id already exists ?
  */
  async entityIdAlreadyExists(entityid){

    let entity = await endpoints_import.endpoints.getEntity(entityid);
    if(!entity){return false}
    return true;
  }

  /**
  Checks if the provided email address is already used by an user
  @param userEmail {String} - An email address
  @return {Boolean} - This email is already used ?
  */
  async userEmailAlreadyExists(userMail){

    let result = true;
    await userModel.find({ email: userMail },

       function(err,user){
          if(user.length==0){
            result = false;
          }
       }

    );

    return result;
  }
}

module.exports =  new security();
