const mongoose = require('mongoose');
const Schema = mongoose.Schema

let Entity = new Schema({
  entityid: {type: Number, required: true},
  type: {type: String, required: true, maxlength:32},
  createdtime: {type: Date, default: Date.now},
  updatedtime: {type: Date, default: Date.now},
  description: {type: String, maxlength:255},
  ownerid: {type: Number},
  creatorid: {type: Number},
  deleted:{type:Boolean, default:false},
  visibility: {type: String, maxlength:10},

});

module.exports = mongoose.model('Entity', Entity);
