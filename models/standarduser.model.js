const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let StandardUser = new Schema({

  standarduserid: {type: Number, required: true},
  name: {type: String, required: true, maxlength:32},
  surname: {type: String, required: true, maxlength:32},
  birthdate: {type: Date, required:true},
  address: {type: String, required: true, maxlength:100},
  city: {type: String, required: true, maxlength:32},

});

module.exports = mongoose.model('StandardUser', StandardUser);
