const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let User = new Schema({

  userid: {type: Number, required: true},
  email: {type: String, required: true, maxlength:32},
  password: {type: String, required: true, maxlength:64},
  usertype: {type: String, required: true, maxlength:32},
  mobilephone: {type: Number, required:true},
  name: {type: String, required: true, maxlength:32},
  surname: {type: String, required: true, maxlength:32},
  birthdate: {type: Date, required: true},
  address: {type: String, default:null, maxlength:100},
  city: {type: String,  default: null, maxlength:32},
  isconnected: {type: Boolean, default:false},
  token:{type:String, maxlength:100},
});

module.exports = mongoose.model('User', User);
