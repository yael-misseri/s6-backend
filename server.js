//Import libraries ************************************************************************
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
//*****************************************************************************************

//Server config ***************************************************************************
const PORT = 4000;
const DATABASE_LOCATION = 'mongodb://127.0.0.1:27017/S6-Aplha';
//*****************************************************************************************


//Import models ***************************************************************************
let entityModel = require('./models/entity.model');
let userModel = require('./models/user.model');
let standardUserModel = require('./models/standarduser.model');
//*****************************************************************************************


//Setup app *******************************************************************************
const app = express();
app.use(cors());
app.use(bodyParser.json());
//*****************************************************************************************


//Define and exports routes ***************************************************************
const entityRoutes = express.Router();
const userRoutes = express.Router();
const standardUserRoutes = express.Router();

app.use('/entity', entityRoutes);
app.use('/user', userRoutes);
app.use('/standardUser', standardUserRoutes);
//*****************************************************************************************


//Connect to DB ***************************************************************************
mongoose.connect(DATABASE_LOCATION, {useNewUrlParser: true, useUnifiedTopology: true});
const connection = mongoose.connection;

connection.once('open', function() {
	console.log("Mongodb database connextion succes");
})

app.listen(PORT, function() {
	console.log("serv run on port: " + PORT);
});
//*******************************************************************************************

//Setup and init endpoints ****************************************************************************
let userEndpoints_import = require('./endpoints/user.endpoints');
const userEndpoints = new userEndpoints_import.userEndpoints(userRoutes,userModel);
userEndpoints.init();

let entityEndpoints_import = require('./endpoints/entity.endpoints');
const entityEndpoints = new entityEndpoints_import.entityEndpoints(entityRoutes,entityModel);
entityEndpoints.init();



//*****************************************************************************************
